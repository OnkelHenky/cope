# ************************************************************
# Sequel Pro SQL dump
# Version 4004
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.1.30)
# Database: cope
# Generation Time: 2013-04-05 15:19:16 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table mode
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mode`;

CREATE TABLE `mode` (
  `silent` tinyint(1) NOT NULL,
  UNIQUE KEY `silent` (`silent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `mode` WRITE;
/*!40000 ALTER TABLE `mode` DISABLE KEYS */;

INSERT INTO `mode` (`silent`)
VALUES
	(0);

/*!40000 ALTER TABLE `mode` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table stats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stats`;

CREATE TABLE `stats` (
  `id` varchar(20) DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  `notif_prompted` varchar(10) DEFAULT NULL,
  `notif_replied` varchar(10) DEFAULT NULL,
  `start` varchar(20) DEFAULT NULL,
  `end` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `stats` WRITE;
/*!40000 ALTER TABLE `stats` DISABLE KEYS */;

INSERT INTO `stats` (`id`, `version`, `notif_prompted`, `notif_replied`, `start`, `end`)
VALUES
	('mnep1339576043892','05','1','1','1365174740480','1365174807563'),
	('mnep1339576043892','05','0','0','1365174730431','1365174736863'),
	('undefined','05','0','0','undefined','1365174723880'),
	('mnep1339576043892','05','0','0','1365172056371','1365172064630');

/*!40000 ALTER TABLE `stats` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tactics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tactics`;

CREATE TABLE `tactics` (
  `id` varchar(20) DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  `timestamp` varchar(20) DEFAULT NULL,
  `URL` varchar(500) DEFAULT NULL,
  `tactic` varchar(5) DEFAULT NULL,
  `answer` varchar(5) DEFAULT NULL,
  `response` varchar(5) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `tactics` WRITE;
/*!40000 ALTER TABLE `tactics` DISABLE KEYS */;

INSERT INTO `tactics` (`id`, `version`, `timestamp`, `URL`, `tactic`, `answer`, `response`, `comments`)
VALUES
	('mnep1339576043892','05','1365174783523','http://www.bbc.co.uk/news/world-asia-22037844','1.2','true','true','kristorena'),
	('mnep1339576043892','05','1365172005532','http://www.bbc.co.uk/news/politics/','5.1','true','true','silent'),
	('mnep1339576043892','05','1365171982220','about:blank','8.1','true','true','silent'),
	('mnep1339576043892','null','1365171928162','http://www.bbc.co.uk/news/uk-21706978','1.2','true','true','silent'),
	('mnep1339576043892','null','1365171884765','http://www.bbc.co.uk/news/magazine-21842721','1.2','true','true','lets s see niw');

/*!40000 ALTER TABLE `tactics` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table urlsequence
# ------------------------------------------------------------

DROP TABLE IF EXISTS `urlsequence`;

CREATE TABLE `urlsequence` (
  `id` varchar(20) DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  `timestamp` varchar(20) DEFAULT NULL,
  `tactic` varchar(5) DEFAULT NULL,
  `A` varchar(250) DEFAULT NULL,
  `B` varchar(250) DEFAULT NULL,
  `C` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `urlsequence` WRITE;
/*!40000 ALTER TABLE `urlsequence` DISABLE KEYS */;

INSERT INTO `urlsequence` (`id`, `version`, `timestamp`, `tactic`, `A`, `B`, `C`)
VALUES
	('mnep1339576043892','05','1365172005532','5.1','http://www.bbc.co.uk/news/business-22038109','http://www.bbc.co.uk/news/politics/','null');

/*!40000 ALTER TABLE `urlsequence` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
